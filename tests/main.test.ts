import 'jest-extended' // for toHaveBeenCalledBefore() to be found by editor
import { TaskMaster } from '../src'

enum TaskType {
    InstrumentCandlesLoadTask = 'task.instrumentCandlesLoad',
    StrategyComputeTask = 'task.strategyCompute',
    BookkeeperAccountUpdateTask = 'task.bookkeeperAccountUpdate',
    SomeOtherTask = 'task.someOther'
}

interface Task {
    type: TaskType
    priority: number
}

interface InstrumentCandlesLoadTask extends Task {
    type: TaskType.InstrumentCandlesLoadTask
    data: {
        instrument: {
            figi: string
        }
        candle: {
            source: string
            interval: string
        }
    }
}

interface StrategyComputeTask extends Task {
    type: TaskType.StrategyComputeTask
    data: {
        strategy: {
            id: string
        }
        params: Record<string, boolean | number | string>
    }
}

interface BookkeeperAccountUpdateTask extends Task {
    type: TaskType.BookkeeperAccountUpdateTask
    data: {
        strategy: {
            id: string
        }
        account: {
            id: string
        }
    }
}

interface SomeOtherTask extends Task {
    // type: 'some-string' // @notice
    type: TaskType.SomeOtherTask
    data: never
}

type Tasks = InstrumentCandlesLoadTask | StrategyComputeTask | BookkeeperAccountUpdateTask

// @notice
// type test = ...

test('main', () => {
    const taskmaster = new TaskMaster<Tasks>()

    const candlesLoadTask = {
        type: TaskType.InstrumentCandlesLoadTask as const, // @notice
        priority: 2,
        data: {
            instrument: {
                figi: 'AAPL-FIGI'
            },
            candle: {
                source: 'tinkoff',
                interval: '1hour'
            }
        }
    }

    const strategyComputeTask = {
        type: TaskType.StrategyComputeTask as const,
        priority: 0,
        data: {
            strategy: {
                id: 'strategy-id'
            },
            params: {
                length: 3,
                multiplier: 1.7
            }
        }
    }

    const accountUpdateTask = {
        type: TaskType.BookkeeperAccountUpdateTask as const,
        priority: 1,
        data: {
            strategy: {
                id: 'strategy-id'
            },
            account: {
                id: 'account-id'
            }
        }
    }

    const someOtherTask = {
        type: TaskType.SomeOtherTask as const,
        priority: 1
    }

    const executorA = jest.fn()
    const workerA = {
        getId: () => 'worker-a',
        getSlots: () => ({
            [TaskType.StrategyComputeTask]: 1,
            [TaskType.BookkeeperAccountUpdateTask]: 1
        }),
        executeTask: (task: Tasks) => executorA(task)
    }

    const executorB = jest.fn()
    const workerB = {
        getId: () => 'worker-b',
        getSlots: () => ({
            [TaskType.InstrumentCandlesLoadTask]: 1
        }),
        executeTask: (task: Tasks) => executorB(task)
    }

    taskmaster.enqueueTask(candlesLoadTask)
    taskmaster.enqueueTask(strategyComputeTask)
    taskmaster.enqueueTask(accountUpdateTask)
    // taskmaster.enqueueTask(someOtherTask) // @notice

    taskmaster.registerWorker(workerA)
    expect(executorA).toHaveBeenCalledBefore(executorB)
    expect(executorA).toHaveBeenNthCalledWith(1, strategyComputeTask)
    expect(executorA).toHaveBeenNthCalledWith(2, accountUpdateTask)

    taskmaster.registerWorker(workerB)
    expect(executorB).toHaveBeenNthCalledWith(1, candlesLoadTask)
})

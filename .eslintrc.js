const path = require('path')

module.exports = {
    parser: '@typescript-eslint/parser',
    plugins: ['@typescript-eslint', 'prettier'],
    extends: [
        'plugin:@typescript-eslint/recommended',
        'plugin:import/errors',
        'plugin:import/warnings',
        'plugin:import/typescript',
        'prettier',
        'prettier/@typescript-eslint'
    ],
    env: {
        es6: true
    },
    settings: {
        'import/resolver': [
            'node',
            {
                webpack: {
                    config: path.resolve(
                        __dirname,
                        'config',
                        'webpack.config.dev.js'
                    )
                }
            }
        ]
    },
    rules: {
        'arrow-parens': ['error', 'as-needed'],
        '@typescript-eslint/no-explicit-any': 'off',
        '@typescript-eslint/explicit-function-return-type': 'off',
        '@typescript-eslint/explicit-member-accessibility': 'off',
        '@typescript-eslint/no-non-null-assertion': 'off',
        'import/order': ['error']
    }
}

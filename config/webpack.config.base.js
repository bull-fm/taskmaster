/* eslint-disable @typescript-eslint/no-var-requires */

const path = require('path')
// const webpack = require('webpack')
const WebpackBar = require('webpackbar')
const nodeExternals = require('webpack-node-externals')
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

// @link: https://medium.com/@christossotiriou/speed-up-nodejs-server-side-development-with-webpack-4-hmr-8b99a932bdda
// @link: https://github.com/microsoft/TypeScript-Node-Starter/blob/master/tsconfig.json

const paths = {
    base: path.resolve(__dirname, '..'),
    src: path.resolve(__dirname, '..', 'src'),
    build: path.resolve(__dirname, '..', 'build'),
    node: {
        modules: path.resolve(__dirname, '..', '..', '..', 'node_modules')
    }
}

// const nodeEnv = process.env.NODE_ENV
const plugins = [
    // new webpack.DefinePlugin({
    //     'process.env': {
    //         NODE_ENV: JSON.stringify(nodeEnv)
    //     }
    // }),
    new CleanWebpackPlugin({ cleanOnceBeforeBuildPatterns: ['**/*'] }),
    new WebpackBar({ profile: true }),
    new FriendlyErrorsWebpackPlugin()
    // new webpack.NamedModulesPlugin()
]

const config = {
    // mode: 'development',
    stats: 'errors-warnings',
    devtool: 'source-map',
    target: 'node',
    externals: [nodeExternals({ modulesDir: paths.node.modules })],

    // To ignore DtsGeneratorPlugin-generated type declaration bundle
    watchOptions: {
        ignored: [paths.build]
    },

    entry: {
        app: path.join(paths.src, 'index.ts')
    },
    output: {
        filename: 'index.js',
        path: paths.build
        // publicPath: './',
        // library: 'chronicle',
        // libraryTarget: 'commonjs2'
    },

    resolve: {
        extensions: ['.js', '.ts'],
        modules: [paths.node.modules],
        alias: {
            '~': paths.src
        }
    },
    plugins,

    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                exclude: /node_modules/,
                loader: ['ts-loader', 'eslint-loader']
            },
            {
                test: /\.js$/,
                enforce: 'pre',
                loader: 'source-map-loader'
            }
        ]
    },

    node: {
        console: false,
        global: false,
        process: false,
        Buffer: false,
        __filename: false,
        __dirname: false
    }
}

module.exports = { paths, config }
